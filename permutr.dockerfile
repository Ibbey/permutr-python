FROM python:3.7-slim

LABEL Author="Ryan Barriger"

COPY . /app

WORKDIR /app

RUN pip3 install -r deploy.txt

WORKDIR /app/App

EXPOSE 8083

ENTRYPOINT [ "python3" ]

CMD [ "app.py" ]



