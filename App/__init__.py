from flask import Flask

app = Flask(__name__, instance_relative_config=True)

# Load the default configuration
app.config.from_object('config.default')

# Load the configuration from the instance folder (overrides/secrets)
app.config.from_pyfile('config.py')

# Load the file specified by the APP_CONFIG_FILE environment variables
app.config.from_envvar('APP_CONFIG_FILE')

# EXAMPLE
# start.sh
#
# export APP_CONFIG_FILE=/var/www/App/config/development.py
# python run.py
