class Echo:
    def __init__(self, identification, message):
        self.id = identification
        self.message = message

    def get_id(self):
        return self.id

    def get_message(self):
        return self.message

    def serialize(self):
        return {
            "id": self.id,
            "message": self.message + " produced by python"
        }
