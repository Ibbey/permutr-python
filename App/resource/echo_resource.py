from flask import jsonify
from flask_restful import Resource
from webargs import fields
from webargs.flaskparser import use_args
from model.echo_model import Echo


class EchoResource(Resource):

    default_args = {'message': fields.Str(missing="empty")}
    _atomic_counter = 0

    @use_args(default_args)
    def get(self, args):
        x = self.get_and_increment_atomic_counter()
        e = Echo(x, args['message'])
        return jsonify(e.serialize())

    def get_atomic_counter(self):
        return type(self)._atomic_counter

    def set_atomic_counter(self, value):
        type(self)._atomic_counter = value

    atomic_counter = property(get_atomic_counter, set_atomic_counter)

    def get_and_increment_atomic_counter(self):
        x = self.atomic_counter
        self.atomic_counter = self.atomic_counter + 1
        return x




