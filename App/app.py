from flask import Flask
from flask_restful import Api
from flask_cors import CORS
from resource.echo_resource import EchoResource

app = Flask(__name__)
cors = CORS(app, resources={r"*": {"origins": "*"}})
api = Api(app)

api.add_resource(EchoResource, '/permutr/v1/pecho')

if __name__ == '__main__':
    app.run("0.0.0.0", port=8083, debug=True)
